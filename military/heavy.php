<?php
/**
 * Created by PhpStorm.
 * User: Aram
 * Date: 31.07.2018
 * Time: 19:07
 */

require_once('militarySpecification.php');

class Heavy extends MilitarySpecification
{
    function __construct() {
        parent::__construct('H', 'Heavy', 5, 4);
    }
}