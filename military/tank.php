<?php
/**
 * Created by PhpStorm.
 * User: Aram
 * Date: 31.07.2018
 * Time: 19:03
 */
require_once('militarySpecification.php');

class Tank extends MilitarySpecification
{
    function __construct() {
        parent::__construct('T', 'Tank', 20, 10);
    }
}