<?php
/**
 * Created by PhpStorm.
 * User: Aram
 * Date: 31.07.2018
 * Time: 19:05
 */

require_once('militarySpecification.php');

class Soldier extends MilitarySpecification
{
    function __construct() {
        parent::__construct('S', 'Soldier', 2, 3);
    }
}