<?php

/**
 * Created by PhpStorm.
 * User: Aram
 * Date: 31.07.2018
 * Time: 19:00
 */

class MilitarySpecification {
    public $key;
    public $name;
    public $health;
    public $attack;

    function __construct($key, $name, $health, $attack) {
        $this->key = $key;
        $this->name = $name;
        $this->health = $health;
        $this->attack = $attack;
    }

    /**
     * is Alive military
     * @return bool
    */
    function isAlive() {
        return $this->health > 0;
    }

    /**
     * @param $unit Unit
     */
    function attackUnit($unit) {
        if ($unit->attack > $this->health) {
            $this->health = 0;
        } else {
            $this->health -= $unit->attack;
        }
    }
}