<?php
/**
 * Created by PhpStorm.
 * User: Aram
 * Date: 31.07.2018
 * Time: 06:42
 */

require_once('militaryEquipment.php');

class Battle
{
    private
        $armyFirst,
        $armySec,
        $firstBuildArmy,
        $secBuildArmy;

    function __construct()
    {
        $armies = $this->catchParameter();

        if (!empty($armies['errorType'])) {
            $this->messages($armies['errorType']);
            return;
        }
        $this->armyFirst = $armies['armyFirst'];
        $this->armySec = $armies['armySec'];
        $this->init();
    }

    /**
     * init (Initialized new parameters, call new function)
     * @return void
     */
    function init()
    {
        $militaryEq = new MilitaryEquipment();
        $buildArmy = $militaryEq->buildArmy([
            'armyFirst' => $this->armyFirst,
            'armySec' => $this->armySec
        ]);

        if (!empty($buildArmy['errorType'])) {
            $this->messages($buildArmy['errorType']);
            return;
        }

        $this->firstBuildArmy = $buildArmy['armyFirst'];
        $this->secBuildArmy = $buildArmy['armySec'];

        $wonMessages = $this->strategy();
        $this->messages($wonMessages);
    }

    /**
     * catchParameter
     * @return array
     */
    function catchParameter()
    {

        if (PHP_SAPI === 'cli') {
            if (!empty($_SERVER['argv'][1])
                && !empty($_SERVER['argv'][2])
            ) {
                $armyFirst = $_SERVER['argv'][1];
                $armySec = $_SERVER['argv'][2];
            }
        } else {
            $armyFirst = $_GET['argument1'];
            $armySec = $_GET['argument2'];
        }

        if (empty($armyFirst)) {
            return ['errorType' => 'errorArmy1'];
        }

        return [
            'armyFirst' => $armyFirst,
            'armySec' => $armySec
        ];
    }

    /**
     * strategy (Strategy all games)
     * @return string
     */
    function strategy()
    {
        $message = '';
        $firstArmy = $this->firstBuildArmy;
        $secArmy = $this->secBuildArmy;
        while (count($secArmy) > 0 && count($firstArmy) > 0) {

            //Army1 shoot
            $randFirstArmy = rand(0, count($firstArmy) - 1);
            $randSecArmy = rand(0, count($secArmy) - 1);
            $firstMilitaryType = $firstArmy[$randFirstArmy];
            $secArmy[$randSecArmy]->attackUnit($firstMilitaryType);

            if (!$secArmy[$randSecArmy]->isAlive()) {
                array_splice($secArmy, $randSecArmy, 1);
            }
            //Army2 shoot
            if (count($secArmy) > 0) {
                $randSecArmy = rand(0, count($secArmy) - 1);
                $randFirstArmy = rand(0, count($firstArmy) - 1);
                $secMilitaryType = $secArmy[$randSecArmy];
                $firstArmy[$randFirstArmy]->attackUnit($secMilitaryType);

                if (!$firstArmy[$randFirstArmy]->isAlive()) {
                    array_splice($firstArmy, $randFirstArmy, 1);
                }
            }

            if (count($secArmy) == 0) {
                echo "Army 1\n" . $this->aliveMilitary($firstArmy);
                $message = 'successWon1';
                break;
            } elseif (count($firstArmy) == 0) {
                echo "Army 2 \n" . $this->aliveMilitary($secArmy);
                $message = 'successWon2';
                break;
            }
        }
        return $message;
    }

    /**
     * show alive military
     * @param $army array
     * @return string string
     */
    function aliveMilitary($army)
    {
        $text = '';
        for ($i = 0; $i < count($army); $i++) {
            $text .= $army[$i]->key . ' :' . $army[$i]->health . "\n";
        }
        return $text;
    }

    /**
     * messages (show success & error messages)
     * @param $messageNumber
     */
    public function messages($messageNumber)
    {
        $messages = [
            'successWon1' => 'Army1 won the battle.',
            'successWon2' => 'Army2 won the battle.',
            'errorArmy1' => 'Armies doesn\'t exist.',
            'errorArmy2' => 'Army2 doesn\'t exist.',
            'errorNotTrueArmy' => 'You don\'t write true parameters, only you can write T, S, H words.',
        ];
        echo $messages[$messageNumber] ?? 'Message does not exist in messages list.';
    }
}

$battle = new Battle();