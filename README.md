**The game structure**

The game process begins with battle.php file. 

In that case, I used Battle, MilitaryEquipment, Tank, Soldier, Heavy, MilitarySpecification Classes.
 
In Battle Class the private $armyFirst, $armySec, $firstBuildArmy, $secBuildArmy parametres are valued in
  __construct(), init() methods. $armyFirst, $armySec parameters contain the keys of armies (T, S, H).
   And the $firstBuildArmy, $secBuildArmy parameters include the arrays of following objects  T, S or H. 
   
   The catchParameter()method gives the  parameters transferred to the game to the $armyFirst, $armySec variables
 In strategy() method in two armies, the random is chosen a shooter and the random who will be shot. 
 During the shooting, health is checked and if it equals to 0 then it is removed from the array.
 
 The aliveMilitary() method shows the whole military of the winning side with its keys and health. 
In messages() method shows the error and success messages which can be occurred during the game. 

In MilitaryEquipment Class exist buildArmy, checkMilitary methods. The buildArmy method with the $armies parameter 
accepts the key combinations of two armies (T, S, H). With the checkMilitary method, new object is created. 
Tank, Soldier, Heavy Classes extend MilitarySpecification where are valued $key, $name, $health, $attack parameters 
and isAlive, attackUnit methods. 

With the __constructor method of Tank, Soldier, Heavy Classes $key, $name, $health, $attack variables are valued.
