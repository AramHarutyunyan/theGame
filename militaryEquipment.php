<?php
/**
 * Created by PhpStorm.
 * User: Aram
 * Date: 31.07.2018
 * Time: 06:54
 */
require_once('military/tank.php');
require_once('military/soldier.php');
require_once('military/heavy.php');

class MilitaryEquipment
{
    /**
     * buildArmy (building firs & sec armies)
     * @param $armies array
     * @return array
     */
    function buildArmy($armies)
    {
        $BuildArmies = [];

        foreach($armies as $armyKey => $army) {
            foreach (str_split($army) as $equipment) {
                $checkMilitary = $this->checkMilitary($equipment);
                if(array_key_exists('errorType',$checkMilitary)) {
                    return $checkMilitary;
                }
                $BuildArmies[$armyKey][] = $checkMilitary;
            }
        }

        return $BuildArmies;
    }

    /**
     * building new military
     * @param $militaryKey
     * @return array|Heavy|Soldier|Tank
     */
    function checkMilitary($militaryKey) {

        if( $militaryKey === 'T') {
            $firstArmy = new Tank();
        } elseif($militaryKey === 'S') {
            $firstArmy = new Soldier();
        }elseIf($militaryKey === 'H') {
            $firstArmy = new Heavy();
        } else {
            return ['errorType' => 'errorNotTrueArmy'];
        }
        return $firstArmy;
    }
}